%global _empty_manifest_terminate_build 0
Name:           python-aiohttp
Version:        3.9.3
Release:        6
Summary:        Async http client/server framework (asyncio)
License:        Apache 2
URL:            https://github.com/aio-libs/aiohttp
Source0:        %{pypi_source aiohttp}
# https://github.com/aio-libs/aiohttp/commit/28335525d1eac015a7e7584137678cbb6ff19397
Patch0:         CVE-2024-27306.patch
# https://github.com/aio-libs/aiohttp/commit/cebe526b9c34dc3a3da9140409db63014bc4cf19
Patch1:         CVE-2024-30251.patch
# https://github.com/aio-libs/aiohttp/commit/7eecdff163ccf029fbb1ddc9de4169d4aaeb6597
Patch2:         CVE-2024-30251-PR-8332-482e6cdf-backport-3.9-Add-set_content_dispos.patch
# https://github.com/aio-libs/aiohttp/commit/f21c6f2ca512a026ce7f0f6c6311f62d6a638866
Patch3:         CVE-2024-30251-PR-8335-5a6949da-backport-3.9-Add-Content-Dispositio.patch
# https://github.com/aio-libs/aiohttp/commit/9ba9a4e531599b9cb2f8cc80effbde40c7eab0bd
Patch4:         Fix-Python-parser-to-mark-responses-without-length-a.patch 
Patch5:         CVE-2024-42367.patch
#https://github.com/aio-libs/aiohttp/commit/259edc369075de63e6f3a4eaade058c62af0df71.patch
Patch6:         CVE-2024-52304.patch

Requires:       python3-attrs
Requires:       python3-charset-normalizer
Requires:       python3-multidict
Requires:       python3-async-timeout
Requires:       python3-yarl
Requires:       python3-frozenlist
Requires:       python3-aiosignal
Requires:       python3-asynctest
Requires:       python3-typing-extensions
Requires:       python3-aiodns
Requires:       python3-Brotli
Requires:       python3-cchardet

%description
Async http client/server framework (asyncio).

%package -n python3-aiohttp
Summary:	Async http client/server framework (asyncio)
Provides:	python-aiohttp
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-cffi
BuildRequires:	gcc
BuildRequires:	gdb
%description -n python3-aiohttp
Async http client/server framework (asyncio).

%package help
Summary:	Development documents and examples for aiohttp
Provides:	python3-aiohttp-doc
%description help
Development documents and examples for aiohttp.

%prep
%autosetup -n aiohttp-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-aiohttp -f filelist.lst
%dir %{python3_sitearch}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Nov 19 2024 changtao <changtao@kylinos.cn> - 3.9.3-6
- Fix CVE-2024-52304

* Fri Oct 11 2024 yaoxin <yao_xin001@hoperun.com> - 3.9.3-5
- Fix CVE-2024-42367

* Mon May 06 2024 xiaozai <xiaozai@kylinos.cn> - 3.9.3-4
- Fix Python parser to mark responses without length as closing

* Mon May 06 2024 yaoxin <yao_xin001@hoperun.com> - 3.9.3-3
- Fix CVE-2024-30251

* Mon Apr 22 2024 yaoxin <yao_xin001@hoperun.com> - 3.9.3-2
- Fix CVE-2024-27306

* Wed Jan 31 2024 yaoxin <yao_xin001@hoperun.com> - 3.9.3-1
- Upgrade to 3.9.3 for fix CVE-2024-23334 and CVE-2024-23829

* Fri Dec 01 2023 wangkai <13474090681@163.com> - 3.9.1-1
- Upgrade to 3.9.1 for fix CVE-2023-47627,CVE-2023-49081,CVE-2023-49082

* Mon Aug 14 2023 yaoxin <yao_xin001@hoperun.com> - 3.8.5-1
- Upgrade to 3.8.5 for fix CVE-2023-37276

* Wed May 17 2023 Ge Wang <wang__ge@126.com> - 3.8.4-1
- update to 3.8.4

* Fri Feb 3 2023 liheavy <lihaiwei8@huawei.com> - 3.8.3-1
- update to 3.8.3

* Fri Jan 20 2023 xu_ping <xuping33@h-partners.com> - 3.7.4-3
- fix install error

* Fri May 20 2022 liukuo <liukuo@kylinos.cn> - 3.7.4-2
- License compliance rectification

* Fri Jul 23 2021 wutao <wutao61@huawei.com> - 3.7.4-1
- Package init
